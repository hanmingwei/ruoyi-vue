package com.ruoyi.file.service;

import java.util.List;

import com.ruoyi.file.domain.FileUpload;
/**
 * 上传文件 服务层
 *
 * @author ruoyi
 */
public interface IFileUploadService {
    /**
     * 根据上传文件ID查询 文件信息
     *
     * @param upload 上传文件ID
     * @return 上传文件信息
     */
    public List<FileUpload> selectFileUploadList(FileUpload upload);

    /**
     * 新增保存上传文件信息
     *
     * @param upload 上传文件信息
     * @return 结果
     */
    public int insertFileUpload(FileUpload upload);

    /**
     * 删除上传文件
     *
     * @param uploadId 上传文件ID
     * @return 结果
     */
    public int deleteUploadById(Long uploadId);

    /**
     * 根据上传文件ID查询信息
     *
     * @param uploadId 上传文件ID
     * @return 上传文件信息
     */
    public FileUpload selectUploadById(Long uploadId);

    /**
     * 修改保存上传文件信息
     *
     * @param upload 上传文件信息
     * @return 结果
     */
    public int updateUpload(FileUpload upload);
}
