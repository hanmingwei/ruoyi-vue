package com.ruoyi.file.service;

import java.util.List;

import com.ruoyi.file.domain.FileFolder;

/**
 * 文件夹管理 服务层
 *
 * @author ruoyi
 */
public interface IFileFolderService {
    public List<FileFolder> selectFileFolderList(FileFolder fileFolder);

    /**
     * 根据文件夹ID查询信息
     *
     * @param folderId 文件夹ID
     * @return 文件夹信息
     */
    public FileFolder selectFolderById(Long folderId);

    /**
     * 校验文件夹名称是否唯一
     *
     * @param folder 文件夹信息
     * @return 结果
     */
    public String checkFolderNameUnique(FileFolder folder);

    /**
     * 修改保存文件夹信息
     *
     * @param folder 文件夹信息
     * @return 结果
     */
    public int updateFolder(FileFolder folder);


    /**
     * 新增保存文件夹信息
     *
     * @param folder 文件夹信息
     * @return 结果
     */
    public int insertFolder(FileFolder folder);


    /**
     * 删除部门管理信息
     *
     * @param folderId 部门ID
     * @return 结果
     */
    public int deleteFolderById(Long folderId);

//
//    /**
//     * 构建前端所需要树结构
//     *
//     * @param depts 部门列表
//     * @return 树结构列表
//     */
//    public List<SysDept> buildDeptTree(List<SysDept> depts);
//
//    /**
//     * 构建前端所需要下拉树结构
//     *
//     * @param depts 部门列表
//     * @return 下拉树结构列表
//     */
//    public List<TreeSelect> buildDeptTreeSelect(List<SysDept> depts);
//
//    /**
//     * 根据角色ID查询部门树信息
//     *
//     * @param roleId 角色ID
//     * @return 选中部门列表
//     */
//    public List<Integer> selectDeptListByRoleId(Long roleId);


//    /**
//     * 根据ID查询所有子部门（正常状态）
//     *
//     * @param deptId 部门ID
//     * @return 子部门数
//     */
//    public int selectNormalChildrenDeptById(Long deptId);
//
//    /**
//     * 是否存在部门子节点
//     *
//     * @param deptId 部门ID
//     * @return 结果
//     */
//    public boolean hasChildByDeptId(Long deptId);
//
//    /**
//     * 查询部门是否存在用户
//     *
//     * @param deptId 部门ID
//     * @return 结果 true 存在 false 不存在
//     */
//    public boolean checkDeptExistUser(Long deptId);
//

//
//    /**
//     * 新增保存部门信息
//     *
//     * @param dept 部门信息
//     * @return 结果
//     */
//    public int insertDept(SysDept dept);
//

}
