package com.ruoyi.file.service.impl;

import com.ruoyi.file.domain.FileFolder;
import com.ruoyi.file.domain.FileUpload;
import com.ruoyi.file.mapper.FileFolderMapper;
import com.ruoyi.file.mapper.FileUploadMapper;
import com.ruoyi.file.service.IFileUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * 上传文件 服务实现
 *
 * @author ruoyi
 */
@Service
public class FileUploadServiceImpl implements IFileUploadService {
    @Autowired
    private FileUploadMapper fileUploadMapper;
    private FileUpload upload;

    /**
     * 根据上传文件ID查询信息
     *
     * @param upload 上传文件ID
     * @return 上传文件信息
     */
    @Override
    public List<FileUpload> selectFileUploadList(FileUpload upload) {
        return fileUploadMapper.selectFileUploadList(upload);
    }

    /**
     * 新增保存上传文件信息
     *
     * @param upload 上传文件信息
     * @return 结果
     */
    @Override
    public int insertFileUpload(FileUpload upload) {
        return fileUploadMapper.insertFileUpload(upload);
    }
    /**
     * 删除上传文件管理信息
     *
     * @param uploadId 上传文件ID
     * @return 结果
     */
    @Override
    public int deleteUploadById(Long uploadId) {
        return fileUploadMapper.deleteUploadById(uploadId);
    }

    /**
     * 根据上传文件ID查询信息
     *
     * @param uploadId 上传文件ID
     * @return 上传文件信息
     */
    @Override
    public FileUpload selectUploadById(Long uploadId) {
        return fileUploadMapper.selectUploadById(uploadId);
    }
    /**
     * 修改保存文件夹信息
     *
     * @param upload 文件夹信息
     * @return 结果
     */
    @Override
    public int updateUpload(FileUpload upload) {
        int result = fileUploadMapper.updateUpload(upload);
        return result;
    }
}
