package com.ruoyi.file.service.impl;

import java.util.List;

import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.file.domain.FileFolder;
import com.ruoyi.file.mapper.FileFolderMapper;
import com.ruoyi.file.service.IFileFolderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.common.annotation.DataScope;


/**
 * 文件夹管理 服务实现
 *
 * @author ruoyi
 */
@Service
public class FileFolderServiceImpl implements IFileFolderService {
    @Autowired
    private FileFolderMapper fileFolderMapper;

    @Override
//    @DataScope(deptAlias = "ff")
    public List<FileFolder> selectFileFolderList(FileFolder fileFolder) {
        return fileFolderMapper.selectFileFolderList(fileFolder);
    }

    /**
     * 根据文件夹ID查询信息
     *
     * @param folderId 文件夹ID
     * @return 文件夹信息
     */
    @Override
    public FileFolder selectFolderById(Long folderId) {
        return fileFolderMapper.selectFolderById(folderId);
    }

    @Override
    public String checkFolderNameUnique(FileFolder folder) {
        return null;
    }

    /**
     * 修改保存文件夹信息
     *
     * @param folder 文件夹信息
     * @return 结果
     */
    @Override
    public int updateFolder(FileFolder folder) {
        FileFolder newParentFolder = fileFolderMapper.selectFolderById(folder.getParentId());
        FileFolder oldFolder = fileFolderMapper.selectFolderById(folder.getFolderId());
//        if (StringUtils.isNotNull(newParentFolder) && StringUtils.isNotNull(oldFolder)) {
//            String newAncestors = newParentFolder.getAncestors() + "," + newParentFolder.getFolderId();
//            String oldAncestors = oldFolder.getAncestors();
//            folder.setAncestors(newAncestors);
//            updateFolderChildren(folder.getFolderId(), newAncestors, oldAncestors);
//        }
        int result = fileFolderMapper.updateFolder(folder);
//        if (UserConstants.DEPT_NORMAL.equals(folder.getStatus())) {
//            // 如果该文件夹是启用状态，则启用该文件夹的所有上级文件夹
//            updateParentDeptStatus(folder);
//        }
        return result;
    }

    /**
     * 新增保存文件夹信息
     *
     * @param folder 文件夹信息
     * @return 结果
     */
    @Override
    public int insertFolder(FileFolder folder) {
        FileFolder info = fileFolderMapper.selectFolderById(folder.getParentId());
        // 如果父节点不为正常状态,则不允许新增子节点
//        if (!UserConstants.DEPT_NORMAL.equals(info.getStatus())) {
//            throw new CustomException("文件夹停用，不允许新增");
//        }
        folder.setAncestors(info.getAncestors() + "," + folder.getParentId());
        return fileFolderMapper.insertFolder(folder);
    }
    /**
     * 删除文件夹管理信息
     *
     * @param folderId 文件夹ID
     * @return 结果
     */
    @Override
    public int deleteFolderById(Long folderId) {
        return fileFolderMapper.deleteFolderById(folderId);
    }


//    @Override
//    public FileFolder selectFolderIdById(Long folderId) {
//        return null;
//    }
//
//    /**
//     * 构建前端所需要树结构
//     *
//     * @param depts 文件夹列表
//     * @return 树结构列表
//     */
//    @Override
//    public List<SysDept> buildDeptTree(List<SysDept> depts) {
//        List<SysDept> returnList = new ArrayList<SysDept>();
//        List<Long> tempList = new ArrayList<Long>();
//        for (SysDept dept : depts) {
//            tempList.add(dept.getDeptId());
//        }
//        for (Iterator<SysDept> iterator = depts.iterator(); iterator.hasNext(); ) {
//            SysDept dept = (SysDept) iterator.next();
//            // 如果是顶级节点, 遍历该父节点的所有子节点
//            if (!tempList.contains(dept.getParentId())) {
//                recursionFn(depts, dept);
//                returnList.add(dept);
//            }
//        }
//        if (returnList.isEmpty()) {
//            returnList = depts;
//        }
//        return returnList;
//    }
//
//    /**
//     * 构建前端所需要下拉树结构
//     *
//     * @param depts 文件夹列表
//     * @return 下拉树结构列表
//     */
//    @Override
//    public List<TreeSelect> buildDeptTreeSelect(List<SysDept> depts) {
//        List<SysDept> deptTrees = buildDeptTree(depts);
//        return deptTrees.stream().map(TreeSelect::new).collect(Collectors.toList());
//    }
//
//    /**
//     * 根据角色ID查询文件夹树信息
//     *
//     * @param roleId 角色ID
//     * @return 选中文件夹列表
//     */
//    @Override
//    public List<Integer> selectDeptListByRoleId(Long roleId) {
//        SysRole role = roleMapper.selectRoleById(roleId);
//        return deptMapper.selectDeptListByRoleId(roleId, role.isDeptCheckStrictly());
//    }
//

//
//    /**
//     * 根据ID查询所有子文件夹（正常状态）
//     *
//     * @param deptId 文件夹ID
//     * @return 子文件夹数
//     */
//    @Override
//    public int selectNormalChildrenDeptById(Long deptId) {
//        return deptMapper.selectNormalChildrenDeptById(deptId);
//    }
//
//    /**
//     * 是否存在子节点
//     *
//     * @param deptId 文件夹ID
//     * @return 结果
//     */
//    @Override
//    public boolean hasChildByDeptId(Long deptId) {
//        int result = deptMapper.hasChildByDeptId(deptId);
//        return result > 0 ? true : false;
//    }
//
//    /**
//     * 查询文件夹是否存在用户
//     *
//     * @param deptId 文件夹ID
//     * @return 结果 true 存在 false 不存在
//     */
//    @Override
//    public boolean checkDeptExistUser(Long deptId) {
//        int result = deptMapper.checkDeptExistUser(deptId);
//        return result > 0 ? true : false;
//    }
//
//    /**
//     * 校验文件夹名称是否唯一
//     *
//     * @param dept 文件夹信息
//     * @return 结果
//     */
//    @Override
//    public String checkDeptNameUnique(SysDept dept) {
//        Long deptId = StringUtils.isNull(dept.getDeptId()) ? -1L : dept.getDeptId();
//        SysDept info = deptMapper.checkDeptNameUnique(dept.getDeptName(), dept.getParentId());
//        if (StringUtils.isNotNull(info) && info.getDeptId().longValue() != deptId.longValue()) {
//            return UserConstants.NOT_UNIQUE;
//        }
//        return UserConstants.UNIQUE;
//    }
//


//
//    /**
//     * 修改该文件夹的父级文件夹状态
//     *
//     * @param dept 当前文件夹
//     */
//    private void updateParentDeptStatus(SysDept dept) {
//        String updateBy = dept.getUpdateBy();
//        dept = deptMapper.selectDeptById(dept.getDeptId());
//        dept.setUpdateBy(updateBy);
//        deptMapper.updateDeptStatus(dept);
//    }
//
//    /**
//     * 修改子元素关系
//     *
//     * @param deptId       被修改的文件夹ID
//     * @param newAncestors 新的父ID集合
//     * @param oldAncestors 旧的父ID集合
//     */
//    public void updateDeptChildren(Long deptId, String newAncestors, String oldAncestors) {
//        List<SysDept> children = deptMapper.selectChildrenDeptById(deptId);
//        for (SysDept child : children) {
//            child.setAncestors(child.getAncestors().replace(oldAncestors, newAncestors));
//        }
//        if (children.size() > 0) {
//            deptMapper.updateDeptChildren(children);
//        }
//    }

//
//    /**
//     * 递归列表
//     */
//    private void recursionFn(List<SysDept> list, SysDept t) {
//        // 得到子节点列表
//        List<SysDept> childList = getChildList(list, t);
//        t.setChildren(childList);
//        for (SysDept tChild : childList) {
//            if (hasChild(list, tChild)) {
//                recursionFn(list, tChild);
//            }
//        }
//    }
//
//    /**
//     * 得到子节点列表
//     */
//    private List<SysDept> getChildList(List<SysDept> list, SysDept t) {
//        List<SysDept> tlist = new ArrayList<SysDept>();
//        Iterator<SysDept> it = list.iterator();
//        while (it.hasNext()) {
//            SysDept n = (SysDept) it.next();
//            if (StringUtils.isNotNull(n.getParentId()) && n.getParentId().longValue() == t.getDeptId().longValue()) {
//                tlist.add(n);
//            }
//        }
//        return tlist;
//    }
//
//    /**
//     * 判断是否有子节点
//     */
//    private boolean hasChild(List<SysDept> list, SysDept t) {
//        return getChildList(list, t).size() > 0 ? true : false;
//    }
//
}
