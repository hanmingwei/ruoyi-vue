package com.ruoyi.file.mapper;

import java.util.List;

import com.ruoyi.file.domain.FileUpload;
/**
 * 上传文件 数据层
 *
 * @author ruoyi
 */

public interface FileUploadMapper {
    /**
     * 根据上传文件ID查询 文件信息
     *
     * @param fileUpload 上传文件ID
     * @return 上传文件信息
     */
    public List<FileUpload> selectFileUploadList(FileUpload fileUpload);

    /**
     * 新增上传文件信息
     *
     * @param fileUpload 文件信息
     * @return 结果
     */
    public int insertFileUpload(FileUpload fileUpload);

//    int insertFileUpload(FileUpload upload);

    /**
     * 删除上传文件管理信息
     *
     * @param uploadId 上传文件ID
     * @return 结果
     */
    public int deleteUploadById(Long uploadId);

    /**
     * 根据上传文件ID查询信息
     *
     * @param uploadId 上传文件ID
     * @return 上传文件信息
     */
    public FileUpload selectUploadById(Long uploadId);

    /**
     * 修改上传文件信息
     *
     * @param upload 上传文件信息
     * @return 结果
     */
    public int updateUpload(FileUpload upload);
}
