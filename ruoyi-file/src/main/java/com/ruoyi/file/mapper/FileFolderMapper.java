package com.ruoyi.file.mapper;

import java.util.List;

import com.ruoyi.file.domain.FileFolder;

/**
 * 文件夹管理 数据层
 *
 * @author ruoyi
 */

public interface FileFolderMapper {

    public List<FileFolder> selectFileFolderList(FileFolder fileFolder);

    /**
     * 根据文件夹ID查询信息
     *
     * @param folderId 文件夹ID
     * @return 文件夹信息
     */
    public FileFolder selectFolderById(Long folderId);

    /**
     * 修改文件夹信息
     *
     * @param folder 文件夹信息
     * @return 结果
     */
    public int updateFolder(FileFolder folder);

    /**
     * 新增文件夹信息
     *
     * @param folder 文件夹信息
     * @return 结果
     */
    public int insertFolder(FileFolder folder);

        /**
     * 删除文件夹管理信息
     *
     * @param folderId 文件夹ID
     * @return 结果
     */
    public int deleteFolderById(Long folderId);


//    /**
//     * 根据ID查询所有子文件夹
//     *
//     * @param deptId 文件夹ID
//     * @return 文件夹列表
//     */
//    public List<SysDept> selectChildrenDeptById(Long deptId);
//
//    /**
//     * 根据ID查询所有子文件夹（正常状态）
//     *
//     * @param deptId 文件夹ID
//     * @return 子文件夹数
//     */
//    public int selectNormalChildrenDeptById(Long deptId);
//
//    /**
//     * 是否存在子节点
//     *
//     * @param deptId 文件夹ID
//     * @return 结果
//     */
//    public int hasChildByDeptId(Long deptId);
//
//


}
