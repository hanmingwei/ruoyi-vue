package com.ruoyi.file.domain;

import com.ruoyi.common.core.domain.BaseEntity;

import java.util.List;

/**
 * 上传文件 file_upload
 *
 * @author ruoyi
 */
public class FileUpload extends BaseEntity {
    private long uploadId;
    private long folderId;
//    private String ancestors;
    private String uploadName;
    private String uploadSize;
    private String uploadUrl;
    private Boolean delFlag;
    private String remark;

    public long getUploadId() {
        return uploadId;
    }

    public void setUploadId(long uploadId) {
        this.uploadId = uploadId;
    }

    public long getFolderId() {
        return folderId;
    }

    public void setFolderId(long folderId) {
        this.folderId = folderId;
    }

//    public String getAncestors() {
//        return ancestors;
//    }
//
//    public void setAncestors(String ancestors) {
//        this.ancestors = ancestors;
//    }

    public String getUploadName() {
        return uploadName;
    }

    public void setUploadName(String uploadName) {
        this.uploadName = uploadName;
    }

    public String getUploadSize() {
        return uploadSize;
    }

    public void setUploadSize(String uploadSize) {
        this.uploadSize = uploadSize;
    }

    public String getUploadUrl() {
        return uploadUrl;
    }

    public void setUploadUrl(String uploadUrl) {
        this.uploadUrl = uploadUrl;
    }

    public Boolean getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Boolean delFlag) {
        this.delFlag = delFlag;
    }

    @Override
    public String getRemark() {
        return remark;
    }

    @Override
    public void setRemark(String remark) {
        this.remark = remark;
    }

}
