package com.ruoyi.file.domain;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 文件夹表 file_folder
 *
 * @author ruoyi
 */
public class FileFolder extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private Long folderId;

    /**
     * 父id
     */
    private Long parentId;

    /**
     * --
     */
    private String ancestors;
    /**
     * 文件夹名称
     */
    private String folderName;

    /**
     * 树形结构 文件夹名称
     */
    private String label;


    /**
     * 路径
     */
    private String path;


    private boolean delFlag = false;


    public Long getFolderId() {
        return folderId;
    }

    public void setFolderId(Long folderId) {
        this.folderId = folderId;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getAncestors() {
        return ancestors;
    }

    public void setAncestors(String ancestors) {
        this.ancestors = ancestors;
    }

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isDelFlag() {
        return delFlag;
    }

    public void setDelFlag(boolean delFlag) {
        this.delFlag = delFlag;
    }
}
