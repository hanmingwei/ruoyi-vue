package com.ruoyi.web.controller.file;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.file.domain.FileFolder;
import com.ruoyi.file.service.IFileFolderService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Iterator;
import java.util.List;

import com.ruoyi.system.service.ISysDeptService;


/**
 * 文件夹controller
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/file/folder")
public class FileFolderController extends BaseController {
    @Autowired
    private IFileFolderService fileFolderService;

    /**
     * 获取文件夹列表
     */
    @PreAuthorize("@ss.hasPermi('file:folder:list')")
    @GetMapping("/list")
    public AjaxResult list(FileFolder fileFolder) {
        List<FileFolder> fileFolders = fileFolderService.selectFileFolderList(fileFolder);
        return AjaxResult.success(fileFolders);
    }

    /**
     * 根据文件夹编号获取详细信息
     */
    @PreAuthorize("@ss.hasPermi('file:folder:query')")
    @GetMapping(value = "/{folderId}")
    public AjaxResult getInfo(@PathVariable Long folderId) {
        return AjaxResult.success(fileFolderService.selectFolderById(folderId));
    }

    /**
     * 修改文件夹
     */
    @PreAuthorize("@ss.hasPermi('file:folder:edit')")
    @Log(title = "文件夹管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Validated @RequestBody FileFolder folder) {
        if (UserConstants.NOT_UNIQUE.equals(fileFolderService.checkFolderNameUnique(folder))) {
            return AjaxResult.error("修改文件夹'" + folder.getFolderName() + "'失败，文件夹名称已存在");
        } else if (folder.getParentId().equals(folder.getFolderId())) {
            return AjaxResult.error("修改文件夹'" + folder.getFolderName() + "'失败，上级文件夹不能是自己");
        }
//        else if (StringUtils.equals(UserConstants.DEPT_DISABLE, folder.getStatus())
//                && fileFolderService.selectNormalChildrenDeptById(folder.getFolderId()) > 0)
//        {
//            return AjaxResult.error("该文件夹包含未停用的子文件夹！");
//        }
        folder.setUpdateBy(SecurityUtils.getUsername());
        return toAjax(fileFolderService.updateFolder(folder));
    }

    /**
     * 查询文件夹列表（排除节点）
     */
    @PreAuthorize("@ss.hasPermi('file:folder:list')")
    @GetMapping("/list/exclude/{folderId}")
    public AjaxResult excludeChild(@PathVariable(value = "folderId", required = false) Long folderId) {
        List<FileFolder> folders = fileFolderService.selectFileFolderList(new FileFolder());
        Iterator<FileFolder> it = folders.iterator();
        while (it.hasNext()) {
            FileFolder ff = (FileFolder) it.next();
//            if (ff.getFolderId().intValue() == folderId
//                    || ArrayUtils.contains(StringUtils.split(ff.getAncestors(), ","), deptId + ""))
            if (ff.getFolderId().intValue() == folderId) {
                it.remove();
            }
        }
        return AjaxResult.success(folders);
    }

    /**
     * 新增文件夹
     */
    @PreAuthorize("@ss.hasPermi('file:folder:add')")
    @Log(title = "文件夹管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Validated @RequestBody FileFolder folder) {
        if (UserConstants.NOT_UNIQUE.equals(fileFolderService.checkFolderNameUnique(folder))) {
            return AjaxResult.error("新增文件夹'" + folder.getFolderName() + "'失败，文件夹名称已存在");
        }
        folder.setCreateBy(SecurityUtils.getUsername());
        return toAjax(fileFolderService.insertFolder(folder));
    }

    /**
     * 删除文件夹
     */
    @PreAuthorize("@ss.hasPermi('file:folder:remove')")
    @Log(title = "文件夹管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{folderId}")
    public AjaxResult remove(@PathVariable Long folderId) {
//        if (fileFolderService.hasChildByDeptId(folderId)) {
//            return AjaxResult.error("存在下级文件夹,不允许删除");
//        }
//        if (fileFolderService.checkDeptExistUser(folderId)) {
//            return AjaxResult.error("文件夹存在用户,不允许删除");
//        }
        return toAjax(fileFolderService.deleteFolderById(folderId));
    }

//
//    /**
//     * 获取文件夹下拉树列表
//     */
//    @GetMapping("/treeselect")
//    public AjaxResult treeselect(SysDept dept)
//    {
//        List<SysDept> depts = deptService.selectDeptList(dept);
//        return AjaxResult.success(deptService.buildDeptTreeSelect(depts));
//    }
//
//    /**
//     * 加载对应角色文件夹列表树
//     */
//    @GetMapping(value = "/roleDeptTreeselect/{roleId}")
//    public AjaxResult roleDeptTreeselect(@PathVariable("roleId") Long roleId)
//    {
//        List<SysDept> depts = deptService.selectDeptList(new SysDept());
//        AjaxResult ajax = AjaxResult.success();
//        ajax.put("checkedKeys", deptService.selectDeptListByRoleId(roleId));
//        ajax.put("depts", deptService.buildDeptTreeSelect(depts));
//        return ajax;
//    }
//


}
