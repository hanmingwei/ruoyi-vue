package com.ruoyi.web.controller.file;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.common.utils.file.FileUtils;
import com.ruoyi.file.domain.FileUpload;
import com.ruoyi.file.service.IFileUploadService;

import jdk.nashorn.internal.objects.Global;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


/**
 * 上传文件controller
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/file/upload")
public class FileUploadController extends BaseController {
    @Autowired
    private IFileUploadService fileUploadService;

    /**
     * 获取文件列表
     *
     * @return
     */
    @PreAuthorize("@ss.hasPermi('file:upload:list')")
    @GetMapping("/list")
    public TableDataInfo list(FileUpload upload) {
        startPage();
        List<FileUpload> list = fileUploadService.selectFileUploadList(upload);
        return getDataTable(list);
    }


//    @PreAuthorize("@ss.hasPermi('file:upload:list')")
//    @GetMapping("/list")
//    public TableDataInfo list(FileUpload fileUpload) {
//        startPage();
//        List<FileUpload> fileUploads = fileUploadService.selectFileUploadList(fileUpload);
//        return AjaxResult.success(fileFolders);
//
////        List<FileUpload> list = fileUploadService.selectFileUploadList(fileUpload);
////        return getDataTable(list);
//    }

    /**
     * 新增上传文件
     */
/*
    @PreAuthorize("@ss.hasPermi('file:upload:add')")
    @Log(title = "文件上传", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Validated @RequestBody FileUpload upload) {
//        if (UserConstants.NOT_UNIQUE.equals(fileUploadService.checkFolderNameUnique(upload))) {
//            return AjaxResult.error("新增上传文件'" + upload.getUploadName() + "'失败，文件名称已存在");
//        }
        upload.setCreateBy(SecurityUtils.getUsername());
        return toAjax(fileUploadService.insertFileUpload(upload));
    }
*/
    @PostMapping("/uploadFile")
    @ResponseBody
    public AjaxResult uploadFile(@RequestParam("file") MultipartFile file, FileUpload fileUpload) throws IOException {
        // 上传文件路径
        String filePath = RuoYiConfig.getUploadPath() ;
        // 上传并返回新文件名称
        String fileName = FileUploadUtils.upload(filePath, file);
        fileUpload.setUploadUrl(fileName);
        return toAjax(fileUploadService.insertFileUpload(fileUpload));
    }

    /**
     * 删除上传文件
     */
    @PreAuthorize("@ss.hasPermi('file:upload:remove')")
    @Log(title = "上传文件管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{uploadId}")
    public AjaxResult remove(@PathVariable Long uploadId) {
        FileUpload fileUpload = fileUploadService.selectUploadById(uploadId);
        // 数据库资源地址
        String path = RuoYiConfig.getProfile() + StringUtils.substringAfter(fileUpload.getUploadUrl(), Constants.RESOURCE_PREFIX);
        FileUtils.deleteFile(path);
        return toAjax(fileUploadService.deleteUploadById(uploadId));
    }

    /**
     * 根据上传文件编号获取详细信息
     */
    @PreAuthorize("@ss.hasPermi('file:upload:query')")
    @GetMapping(value = "/{uploadId}")
    public AjaxResult getInfo(@PathVariable Long uploadId) {
        return AjaxResult.success(fileUploadService.selectUploadById(uploadId));
    }

    /**
     * 修改上传文件
     */
    @PreAuthorize("@ss.hasPermi('file:upload:edit')")
    @Log(title = "上传文件管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Validated @RequestBody FileUpload upload) {
        upload.setUpdateBy(SecurityUtils.getUsername());
        return toAjax(fileUploadService.updateUpload(upload));
    }


    @GetMapping("/downloadFile")
    public void resourceDownload(@RequestParam("id") Long id, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        FileUpload fileUpload = fileUploadService.selectUploadById(id);
        // 数据库资源地址

        String downloadPath = RuoYiConfig.getProfile() + StringUtils.substringAfter(fileUpload.getUploadUrl(), Constants.RESOURCE_PREFIX);
        //FileUtils.deleteFile(downloadPath);  删除需要

        // 下载名称
        String downloadName = StringUtils.substringAfterLast(downloadPath, "/");
        String type = StringUtils.substringAfter(downloadName, ".");
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition",
                "attachment;fileName=" + FileUtils.setFileDownloadHeader(request, fileUpload.getUploadName()+"."+type));
        FileUtils.writeBytes(downloadPath, response.getOutputStream());
    }

}
