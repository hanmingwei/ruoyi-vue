import request from '@/utils/request'

// 查询部门列表
export function listFolder(query) {
  return request({
    url: '/file/folder/list',
    method: 'get',
    params: query
  })
}

// 查询部门列表（排除节点）
export function listFolderExcludeChild(folderId) {
  return request({
    url: '/file/folder/list/exclude/' + folderId,
    method: 'get'
  })
}

// 查询部门详细
export function getFolder(folderId) {
  return request({
    url: '/file/folder/' + folderId,
    method: 'get'
  })
}

// 查询部门下拉树结构
export function treeselect() {
  return request({
    url: '/file/folder/treeselect',
    method: 'get'
  })
}

// 根据角色ID查询部门树结构
export function roleFolderTreeselect(roleId) {
  return request({
    url: '/file/folder/roleFolderTreeselect/' + roleId,
    method: 'get'
  })
}

// 新增部门
export function addFolder(data) {
  return request({
    url: '/file/folder',
    method: 'post',
    data: data
  })
}

// 修改部门
export function updateFolder(data) {
  return request({
    url: '/file/folder',
    method: 'put',
    data: data
  })
}

// 删除部门
export function delFolder(folderId) {
  return request({
    url: '/file/folder/' + folderId,
    method: 'delete'
  })
}